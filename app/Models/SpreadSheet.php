<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpreadSheet extends Model
{
    use HasFactory;

    // スプレッドシート挿入用Function
    static function insert_spread_sheet($insert_data)
    {
        // スプレッドシートを操作するGoogleClientインスタンスの生成
        $sheets = SpreadSheet::instance();

        // https://docs.google.com/spreadsheets/d/×××××××××××××××××××/edit#gid=0
        $sheet_id = '1f_jgcRVIyH0vz2-_OdILQAFCqm-zV-0Omc3Kyp8JK9w';
        $range = 'A1:A';
        $response = $sheets->spreadsheets_values->get($sheet_id, $range);
        // 格納する行の計算
        $row = count($response->getValues()) + 1;

        // データを整形（この順序でシートに格納される）
        $contact = [
            $insert_data['name'],
            $insert_data['birthday'],
            $insert_data['sex'],
            $insert_data['email'],
            $insert_data['password'],
        ];
        $values = new \Google_Service_Sheets_ValueRange();
        $values->setValues([
            'values' => $contact
        ]);
        $sheets->spreadsheets_values->append(
            $sheet_id,
            'A'.$row,
            $values,
            ["valueInputOption" => 'USER_ENTERED']
        );

        return true;
    }

    // スプレッドシート操作用のインスタンスを生成するFunction
    public static function instance() {
        $credentials_path = storage_path('app/json/credentials.json');
        $client = new \Google_Client();
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAuthConfig($credentials_path);
        return new \Google_Service_Sheets($client);
    }
}
