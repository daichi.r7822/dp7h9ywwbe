<?php

namespace App\Helpers;

use DateTime;
use DateInterval;
use DatePeriod;
use Illuminate\Support\Facades\DB;

class Common
{
    /* 
    reservedテーブル,retentionテーブルに格納されている日付データを１日区切りにして返す処理
    呼び出し方法
    use App\Helpers\Common;
    Common::unusableDatesSplit()
    */
    public static function unusableDatesSplit()
    {
        $reserved = DB::select('select fromdate,todate from reserved');
        $retention = DB::select('select fromdate,todate from retention');
        $items = array_merge($reserved, $retention);
        $dates = [];
        foreach($items as $item) {
            $start_date = new DateTime($item->fromdate);
            $end_date = new DateTime($item->todate);
            // DateTimeで計算すると１日切り上げられるので、＋１で丁度いい
            $end_date = $end_date->modify('+1 days');
            $interval = new DateInterval('P1D');
            $date_range = new DatePeriod($start_date, $interval, $end_date);
            
            foreach($date_range as $date){
                array_push($dates, $date->format('Y-m-d'));
            }
        }

        return $dates;
    }

    /* 
    引数で受けた日付データを１日区切りにして返す処理
    呼び出し方法
    use App\Helpers\Common;
    Common::dateSplit()
    */
    public static function dateSplit($fromdate, $todate)
    {
        $dates = [];
        $start_date = new DateTime($fromdate);
        $end_date = new DateTime($todate);
        // DateTimeで計算すると１日切り上げられるので、＋１で丁度指定した日付全てを処理できる
        $end_date = $end_date->modify('+1 days');
        $interval = new DateInterval('P1D');
        $date_range = new DatePeriod($start_date, $interval, $end_date);
        
        foreach($date_range as $date){
            array_push($dates, $date->format('Y-m-d'));
        }

        return $dates;
    }
}
