<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Helpers\Common;


class ReservedDateController extends Controller
{
    // 予約ページ用
    public function index(Request $request)
    {
        $items = Common::unusableDatesSplit();
        // 下記コードプラグインカレンダーバグ用の打開策(disabledDaysオプションで最後の日付が反映されないので、ダミー日付を挿入)
        array_push($items, "3000-01-01");
        $items = json_encode($items);
        return view('dashboard', ['items' => $items]);
    }

    // 予約DB最終確認＆保持or失敗
    public function confirm(Request $request)
    {
        $unusableDates = Common::unusableDatesSplit();
        $splitdates = Common::dateSplit($request->fromdate, $request->todate);

        $result = array_intersect($splitdates, $unusableDates);
        
        if ($result) {
            $request->session()->flash('alert', '他のお客様が予約している日程が選択されているため予約できません。');
            return redirect('/dashboard');
        } else {

            // 人数を引数に取り合計金額の算出
            $price = $this->money($request->person, $request->fromdate, $request->todate);

            // retentionにデータ保持
            $id = Auth::id();
            $param = [
                'account_id' => $id,
                'fromdate' => $request->fromdate,
                'todate' => $request->todate,
                'person' => $request->person,
                'totalMoney' => $price,
            ];
            DB::insert('insert into retention (account_id, fromdate, todate, person, totalMoney) values (:account_id, :fromdate, :todate, :person, :totalMoney)', $param);
            
            // stripe決済処理
            $splitdates = Common::dateSplit($request->fromdate, $request->todate);
            $lineItems = [
                'price_data' => [
                    'unit_amount' => $price,
                    'currency' => 'JPY',

                    'product_data' => [
                        'name' => 'ダミー名前',
                        'description' => '予約日',
                    ],
                ],
                'quantity' => 1,
            ];

            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

            $session = $checkout_session = \Stripe\Checkout\Session::create([
                'payment_method_types' => ['card'],
                'line_items' => [$lineItems],
                'mode' => 'payment',
                'success_url' => route('success'),
                'cancel_url' => route('cancel'),
            ]);

            $publicKey = env('STRIPE_PUBLIC_KEY');
            
            return view('checkout', compact('session', 'publicKey'));
        }
    }

    // 決済成功時の処理
    public function success()
    {
        $id = Auth::id();
        $items = DB::table('retention')->select('fromdate', 'todate', 'person')->where('account_id', $id)->get();
        $this->create($items[0]->person, $items[0]->fromdate, $items[0]->todate);
        DB::table('retention')->where('account_id', $id)->delete();
        return redirect()->route('reserved-date');
    }

    // 決済失敗時の処理
    public function cancel()
    {
        $id = Auth::id();
        DB::table('retention')->where('account_id', $id)->delete();
        return redirect()->route('dashboard');
    }

    // 予約情報をreservedに追加
    public function create($person, $fromdate, $todate)
    {
        $id = Auth::id();
        // 人数を引数に取り合計金額の算出
        $totalMoney = $this->money($person, $fromdate, $todate);
        $param = [
            'account_id' => $id,
            'fromdate' => $fromdate,
            'todate' => $todate,
            'person' => $person,
            'totalMoney' => $totalMoney,
        ];
        //DBに接続しデータを挿入する。第１パラメータにSQL文、第２に$paramを。
        DB::insert('insert into reserved (account_id, fromdate, todate, person, totalMoney) values (:account_id, :fromdate, :todate, :person, :totalMoney)', $param);
    }

    // retensionから情報を削除

    // 予約確認ページ用
    public function bookingConfirmation()
    {
        $id = Auth::id();
        $items = DB::table('reserved')->select('fromdate', 'todate', 'person', 'totalMoney')->where('account_id', $id)->get();
        return view('reserved-date', ['items' => $items]);
    }

    // 金額計算用
    public function money($personNumber, $fromdate, $todate)
    {
        // 人数から１泊の料金を算出
        if (0>=$personNumber) {
            $totalMoney = null;
        } elseif($personNumber<=6) {
            $totalMoney = 77000;
        } else {
            $refs = $personNumber - 6;
            $totalMoney = ($refs * 11000) + 77000;
        }

        // 宿泊日数を算出
        $carbonTodate = new Carbon($todate);
        $carbonFromdate = new Carbon($fromdate);
        $totalDate = $carbonTodate->diffInDays($carbonFromdate);

        return $totalMoney * $totalDate;
    }
}
