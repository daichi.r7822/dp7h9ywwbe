<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use App\Http\Controllers\SpreadSheetController;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        // 生年月日を整形
        $birthday = $request->birthYear . '年' . $request->birthMonth . '月' . $request->birthDay . '日';
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'birthYear' => ['required', 'string'],
            'birthMonth' => ['required', 'string'],
            'birthDay' => ['required', 'string'],
            'sex' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'birthday' => $birthday,
            'sex' => $request->sex,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        // スプレットシートにユーザー情報落とし込み処理
        SpreadSheetController::store([
            'name' => $request->name,
            'birthday' => $birthday,
            'sex'  => $request->sex,
            'email'  => $request->email,
            'password'  => $request->password,
        ]);

        return redirect(RouteServiceProvider::HOME);
    }
}
