<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SpreadSheet;

class SpreadSheetController extends Controller
{
    static function store($data)
    {
        $spread_sheet = new SpreadSheet();

        // スプレッドシートに格納するデータ
        $insert_data = [
            'name' => $data['name'],
            'birthday' => $data['birthday'],
            'sex'  => $data['sex'],
            'email'  => $data['email'],
            'password'  => $data['password'],
        ];

        $spread_sheet->insert_spread_sheet($insert_data);

        //トップページに遷移する
        return redirect('dashboard');
    }
}