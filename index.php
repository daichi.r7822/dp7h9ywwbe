<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jqueryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="./css/login.css">
    <script src="./js/login.js"></script>
    <title>Document</title>
</head>
<body>
<div class="container" id="container">
  <div class="form-container sign-up-container">
    <form action="./php/register.php" method="post">
      <h1>新規登録</h1>
      <!-- TODO google,facebook等でログインできるようになったら使用 -->
      <!-- <div class="social-container">
        <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
        <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
        <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
      </div> -->
      <!-- UI相談 -->
      <!-- <span>or use your email for registration</span> -->
      <input type="text" placeholder="Name" name="name" />
      <input type="email" placeholder="Email" name="email" />
      <input type="password" placeholder="Password" name="password" />
      <button type="submit">新規登録</button>
    </form>
  </div>
  <div class="form-container sign-in-container">
    <form action="./php/login.php" method="post">
      <h1>ログイン</h1>
      <!-- TODO google,facebook等でログインできるようになったら使用 -->
      <!-- <div class="social-container">
        <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
        <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
        <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
      </div> -->
      <!-- UI相談 -->
      <!-- <span>or use your account</span> -->
      <input type="email" placeholder="Email" name="email" />
      <input type="password" placeholder="Password" name="password" />
      <a href="#">パスワードを忘れた方</a>
      <button type="submit">ログイン</button>
    </form>
  </div>
  <div class="overlay-container">
    <div class="overlay">
      <div class="overlay-panel overlay-left">
        <h1>ログイン</h1>
        <p>個人情報を入力してログインしてください。</p>
        <button class="ghost" id="signIn">Sign In</button>
      </div>
      <div class="overlay-panel overlay-right">
        <h1>新規登録</h1>
        <p>お客様情報を入力して、旅を始めましょう。</p>
        <button class="ghost" id="signUp">Sign Up</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>