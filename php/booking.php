<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- datedropper -->
    <script src="../js/datedropper-javascript.js"></script>
    <title>Document</title>
</head>
<body>
<form action="confirm.php" method="post" name="form">
  <input name="fromdate" type="text">
  <input name="todate" type="text">
  <button type="submit">送信</button>
</form>
<script>
new dateDropper({
  selector: 'input[type="text"]',
  range: true,
  doubleView: true,
//   format: 'y-m-d',
  showArrowsOnHover: false,
//   customClass: 'testClass' // モーダルカレンダーにクラス名付与
//   overlay: true,// UI相談
  maxYear: <?= date('Y')?>+5, // max=現在の年＋５年
  minYear: <?= date('Y')?>, // min=現在の年
  jump: 5, // 年をクリックしたときに１年刻みで表示
//   startFromMonday: true, // UI相談
//   maxDate: "<?= date('Y')?>+5/12/31",
//   minDate: "2021/01/01", // 現在の一カ月先を指定すれば、一般客の制御完了
//   disabledDays: "2022/12/14,2022/12/15", // 予約済みの日にちの無効化
//   maxRange: 3, // 最長何泊できるかの制御→相談
//   disabledWeekDays: '0,6', // 使用できない曜日を指定できる０～月曜日～
});
</script>
</body>
</html>