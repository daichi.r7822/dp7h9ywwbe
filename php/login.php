<?php
// ログイン情報をPOSTで取得
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email       = $_POST["email"];
    $password      = $_POST["password"];
}

// 必要情報の読み込み
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Users\UserInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;


require __DIR__ .  '/../vendor/autoload.php';

$capsule = new Capsule;

// データベースの設定
$capsule->addConnection([
  'driver'  => 'mysql',
  'host'   => 'localhost',
  'database' => 'mangata',
  'username' => 'mangata',
  'password' => 'password123',
  'charset'  => 'utf8',
  'collation' => 'utf8_unicode_ci',
]);

$capsule->bootEloquent();

// ユーザー情報の設定
$credentials = [
'email' => $email,
'password' => $password,
];

// 登録済みかを確認
$user = Sentinel::authenticate($credentials, true);

if (is_null($user)) {
    echo "アカウントが登録されていません。";
} else {
  header('Location: http://localhost/mangata/php/booking.php');
}

?>