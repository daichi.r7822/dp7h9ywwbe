<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'テスト１',
                'email' => 'test1@test.com',
                'birthday' => '2001年10月14日',
                'sex' => '男性',
                'password' => Hash::make('password123'),
                'created_at' => '2023/01/01 11:11:11'
            ],
            [
                'name' => 'テスト２',
                'email' => 'test2@test.com',
                'birthday' => '1999年1月4日',
                'sex' => '男性',
                'password' => Hash::make('password123'),
                'created_at' => '2023/01/01 11:11:11'
            ],
            [
                'name' => 'テスト３',
                'email' => 'test3@test.com',
                'birthday' => '1987年8月7日',
                'sex' => '女性',
                'password' => Hash::make('password123'),
                'created_at' => '2023/01/01 11:11:11'
            ],
            [
                'name' => 'テスト４',
                'email' => 'test4@test.com',
                'birthday' => '1789年12月14日',
                'sex' => '男性',
                'password' => Hash::make('password123'),
                'created_at' => '2023/01/01 11:11:11'
            ],
            [
                'name' => 'テスト５',
                'email' => 'test5@test.com',
                'birthday' => '2000年1月1日',
                'sex' => '女性',
                'password' => Hash::make('password123'),
                'created_at' => '2023/01/01 11:11:11'
            ],
        ]);
    }
}
