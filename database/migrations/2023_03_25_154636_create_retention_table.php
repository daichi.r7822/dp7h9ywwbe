<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetentionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retention', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id')->constrained('users'); // 外部キー制約
            $table->string('fromdate')->nullable();
            $table->string('todate')->nullable();
            $table->integer('person')->nullable();
            $table->integer('totalMoney')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retention');
    }
}
