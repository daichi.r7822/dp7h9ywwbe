<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- datedropper.js -->
    <script src="../js/datedropper-javascript.js"></script>
    <!-- datedropper.css -->
    <link rel="stylesheet" href="./css/datedropper.css">
    <title>Document</title>
</head>

<body>
    <!-- laravelデフォルト -->
    <x-app-layout>
        <x-slot name="header">
            <!-- 予約できない日程が選択されていた場合のflashメッセージ -->
            @if(session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif
            <!-- 予約フォーム -->
            <form method="POST" action="reserved">
                @csrf
                <!-- カレンダー -->
                <input name="fromdate" type="text">
                <input name="todate" type="text">
                <br>
                <!-- 人数選択 -->
                <select name="person">
                    <?php for ($number = 1; $number <= 20; $number++) : ?>
                        <option><?= $number ?></option>
                    <?php endfor; ?>
                </select>人
                <br>
                <!-- 予約フォーム送信 -->
                <button type="submit">送信</button>
            </form>
            

        </x-slot>
    </x-app-layout>
    <script>

        // カレンダー
        new dateDropper({
            selector: 'input[type="text"]',
            range: true,
            doubleView: true,
            showArrowsOnHover: false,
            //   customClass: 'testClass' // モーダルカレンダーにクラス名付与
            //   overlay: true,// UI相談
            maxYear: <?= date('Y') ?> + 5, // max=現在の年＋５年
            minYear: <?= date('Y') ?>, // min=現在の年
            jump: 5, // 年をクリックしたときに１年刻みで表示
            //   startFromMonday: true, // UI相談
            //   maxDate: "<?= date('Y') ?>+5/12/31",
            //   minDate: "2021/01/01", // 現在の一カ月先を指定すれば、一般客の制御完了
            disabledDays: JSON.stringify(<?= $items ?>), // 予約済みの日にちの無効化
            //   maxRange: 3, // 最長何泊できるかの制御→相談
            //   disabledWeekDays: '0,6', // 使用できない曜日を指定できる０～月曜日～
        });
    </script>
</body>

</html>