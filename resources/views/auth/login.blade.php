<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jqueryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="./css/login.css">
    <script src="./js/login.js"></script>
    <title>Document</title>
</head>

<body>
    <x-slot name="logo">
        <a href="/">
            <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
        </a>
    </x-slot>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />
    <!-- Validation Errors -->
    <x-auth-validation-errors class="validate" :errors="$errors" />
    <div class="container" id="container">
        <!-- 新規登録フォーム -->
        <div class="form-container sign-up-container">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <h1>新規登録</h1>
                <!-- TODO google,facebook等でログインできるようになったら使用 -->
                <!-- <div class="social-container">
                        <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                        <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                        </div> -->
                <!-- UI相談 -->
                <!-- <span>or use your email for registration</span> -->


                <!-- 名前 -->
                <input type="text" placeholder="Name" name="name" />
                <!-- 生年月日 -->
                <div>
                    <select name="birthYear" id="birthYear" class="age">
                        <option></option>
                        <?php for ($year = date('Y') - 90; $year <= date('Y') - 18; $year++) : ?>
                            <option><?= $year ?></option>
                        <?php endfor; ?>
                    </select>年
                    <select name="birthMonth" id="birthMonth" class="age">
                        <option></option>
                        <?php for ($month = 1; $month <= 12; $month++) : ?>
                            <option><?= $month ?></option>
                        <?php endfor; ?>
                    </select>月
                    <select name="birthDay" id="birthDay" class="age">
                        <option></option>
                        <?php for ($day = 1; $day <= 31; $day++) : ?>
                            <option><?= $day ?></option>
                        <?php endfor; ?>
                    </select>日
                </div>
                <!-- 性別 -->
                <div>
                    <input class="sex" type="radio" name="sex" id="male" value="男性">男性
                    <input class="sex" type="radio" name="sex" id="female" value="女性">女性
                    <input class="sex" type="radio" name="sex" id="sex-others" value="指定無し">指定無し
                </div>
                <!-- e-mail -->
                <input type="email" placeholder="Email" name="email" />
                <!-- password -->
                <input type="password" placeholder="Password" name="password" />
                <input type="password" placeholder="password_confirmation" name="password_confirmation" />
                <!-- 送信 -->
                <button type="submit" class="register-button">新規登録</button>
            </form>
        </div>
        <!-- ログインフォーム -->
        <div class="form-container sign-in-container">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <h1>ログイン</h1>
                <!-- TODO google,facebook等でログインできるようになったら使用 -->
                <!-- <div class="social-container">
                <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                </div> -->
                <!-- UI相談 -->
                <!-- <span>or use your account</span> -->
                <input type="email" placeholder="Email" name="email" />
                <input type="password" placeholder="Password" name="password" />
                <a href="{{ route('password.request') }}">パスワードを忘れた方</a>
                <button type="submit">ログイン</button>
            </form>
        </div>
        <!-- 切り替え側画面 -->
        <div class="overlay-container">
            <div class="overlay">
                <div class="overlay-panel overlay-left">
                    <h1>ログイン</h1>
                    <p>個人情報を入力してログインしてください。</p>
                    <button class="ghost" id="signIn">Sign In</button>
                </div>
                <div class="overlay-panel overlay-right">
                    <h1>新規登録</h1>
                    <p>お客様情報を入力して、旅を始めましょう。</p>
                    <button class="ghost" id="signUp">Sign Up</button>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>

</html>