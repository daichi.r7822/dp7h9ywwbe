<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- datedropper.js -->
    <script src="../js/datedropper-javascript.js"></script>
    <!-- datedropper.css -->
    <link rel="stylesheet" href="./css/datedropper.css">
    <title>Document</title>
</head>

<body>
    <!-- laravelデフォルト -->
    <x-app-layout>
        <x-slot name="header">
            <!-- 予約日確認 -->
            @foreach($items as $item)
              <p>ご予約日：{{ $item->fromdate }}〜{{ $item->todate }}</p>
              <p>ご予約人数：{{ $item->person }}人</p>
              <p>合計金額：{{ number_format($item->totalMoney) }}円<p>
              <br>
            @endforeach
        </x-slot>
    </x-app-layout>
    <script>
        // 範囲指定の日にちを細分化
        var dateperiod = function() {
            var dateList = [];
            <?php foreach ($items as $item) : ?>
                var fromDate = new Date("<?= $item->fromdate ?>");
                var toDate = new Date("<?= $item->todate ?>");

                for (var d = fromDate; d <= toDate; d.setDate(d.getDate() + 1)) {
                    var formatedDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
                    dateList.push(formatedDate);
                }
                var formatedDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
                dateList.push(formatedDate);
            <?php endforeach; ?>
            return JSON.stringify(dateList);
        }
    </script>
</body>

</html>