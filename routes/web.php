<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ReservedDateController;
use App\Http\Controllers\SpreadSheetController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ログイン＆新規登録画用 */
Route::get('/', function () {
    return view('auth.login');
});

/* スプレッドシート書き込み用 */
Route::get('/sheet', [SpreadSheetController::class, 'store']);

/* パスワードリセット用 */
Route::get('/forgot-password', function () {
    return view('auth.forgot-password');
})->middleware('guest')->name('password.request');

/* ログイン後 */
Route::middleware(['verified'])->group(function (){
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    /* 予約日DBインサート用 */
    Route::get('post', [ReservedDateController::class, 'post']);
    Route::post('reserved', [ReservedDateController::class, 'confirm']);

    /* 予約日DB取り出し用 */
    Route::get('dashboard', [ReservedDateController::class, 'index'])->name('dashboard');

    /* 予約済み確認ページ */
    Route::get('/reserved-date', [ReservedDateController::class, 'bookingConfirmation'])->name('reserved-date');

    /* stripe決済 */
    Route::get('checkout', [ReservedDateController::class, 'checkout'])->name('checkout');

    /* stripe決済成功時 */
    Route::get('success', [ReservedDateController::class, 'success'])->name('success');

    /* stripe決済失敗時 */
    Route::get('cancel', [ReservedDateController::class, 'cancel'])->name('cancel');
});


require __DIR__.'/auth.php';
